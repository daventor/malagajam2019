﻿using Rewired;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    #region Variables
    PlayerManager plManager;

    Plane ground;
    private Rewired.Player rewiredPlayer;
    #endregion

    public void Init(PlayerManager _plManager) {
        plManager = _plManager;
        rewiredPlayer = ReInput.players.GetPlayer(0);        
    }

    public void Tick(float delta) {
        UpdateStates();
    }

    void UpdateStates() {
        //if (playerActions.Escape.WasPressed || playerActions.Start.WasPressed) {
        //    GameController.Instance.hudManager.pauseMenu.SwitchPause();
        //}

        if (!plManager.canControl) {            
            plManager.states.moveAmount = 0;
            return;
        }

        Vector3 v = rewiredPlayer.GetAxisRaw("MovY") * GameManager.Instance.mainCamera.transform.up;
        Vector3 h = rewiredPlayer.GetAxisRaw("MovX") * GameManager.Instance.mainCamera.transform.right;

        plManager.states.moveDir = (v + h).normalized;
        float m = Mathf.Abs(rewiredPlayer.GetAxisRaw("MovX")) + Mathf.Abs(rewiredPlayer.GetAxisRaw("MovY"));
        plManager.states.moveAmount = Mathf.Clamp01(m);

        if (rewiredPlayer.GetButtonDown("Fire")) {
            plManager.states.DoFire();
        }
    }
}
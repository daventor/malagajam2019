﻿using UnityEngine;

public class StateManager : MonoBehaviour {

    #region Variables        
    public float speed = 10;     

    [HideInInspector]    
    public float moveAmount;
    [HideInInspector]
    public Vector3 moveDir;    

    public bool canMove = true;

    public Animator anim;
    
    Rigidbody rb;    
    #endregion

    #region Unity Methods

    public void Init() {        
        rb = GetComponent<Rigidbody>();        
    }

    public void Tick(float delta) {        
        if (Time.timeScale <= float.Epsilon) return;
        if (GameManager.Instance.currentGameState != GameStateType.inGame) return;

        moveDir.y = 0;
        if (canMove) {
            rb.velocity = (moveDir * (speed * moveAmount));
            GameManager.Instance.player.myAnimator.speed = moveAmount;
        } else {
            GameManager.Instance.player.myAnimator.speed = 0;
            rb.velocity = Vector3.zero;
        }        

        if (canMove) {
            Quaternion tr = Quaternion.LookRotation(((moveDir != Vector3.zero) ? moveDir:transform.forward));
            
            transform.rotation = tr;
        }        
    }

    public void DoFire() {
        if (canMove && GameManager.Instance.currentGameState == GameStateType.inGame) {
            if (GameManager.Instance.player.interactibleTarget != null) GameManager.Instance.player.interactibleTarget.ActiveAction();
        }
    }

    #endregion  
}
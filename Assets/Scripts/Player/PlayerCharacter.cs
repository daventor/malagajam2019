﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{    
    [Header("Player values")]

	public Gauge money;
    public Gauge social;
    public Gauge family;
    public Gauge happiness;

    public bool man = true;
        
	public bool godmode;    
    
    public void Init() {

        money.CurrentValue = social.CurrentValue = family.CurrentValue = happiness.CurrentValue = 0.5f;
		money = new Gauge(0.5f, 1);
		social = new Gauge(0.5f, 1);
		family = new Gauge(0.5f, 1);
		happiness = new Gauge(0.5f, 1);
	}

	public void ChangePlayerValues(Result resultEvent)
	{
		this.money.CurrentValue += resultEvent.money;
		this.social.CurrentValue += resultEvent.social;
		this.family.CurrentValue += resultEvent.family;
		this.happiness.CurrentValue += resultEvent.happiness;

		GameManager.Instance.statGUIManager.UpdateSliders();

        float lifeMeaning = (1 - ((money.CurrentValue + social.CurrentValue + family.CurrentValue + happiness.CurrentValue) / 4)) * -100;

        GameManager.Instance.cameraManager.SetSaturation(lifeMeaning);

        if (!godmode) { 
		    if (money.CurrentValue <= 0) GameManager.Instance.gameStateManager.GameOver(GameStateManager.GameOverType.Money);
		    else if (social.CurrentValue <= 0) GameManager.Instance.gameStateManager.GameOver(GameStateManager.GameOverType.Social);
		    else if (family.CurrentValue <= 0) GameManager.Instance.gameStateManager.GameOver(GameStateManager.GameOverType.Family);
		    else if (happiness.CurrentValue <= 0) GameManager.Instance.gameStateManager.GameOver(GameStateManager.GameOverType.Happiness);
        }

        // Check stats level
        if (money.CurrentValue < 0.25f || social.CurrentValue < 0.25f || family.CurrentValue < 0.25f || happiness.CurrentValue < 0.25f) {
            GameManager.Instance.audioManager.CrossFade("BGMusicGood", "BGMusicBad");
        } else {
            GameManager.Instance.audioManager.CrossFade("BGMusicBad", "BGMusicGood");
        }


    }
}
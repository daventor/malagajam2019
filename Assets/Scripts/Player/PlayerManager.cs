﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    public bool canControl = true;
    public GameObject minimapMarker;
    public GameObject manModel;
    public GameObject womenModel;
    public GameObject manLight;
    public GameObject womenLight;

    public GameObject changeSexParticles;
    public GameObject starParticles;

    [HideInInspector]
    public StateManager states;
    [HideInInspector]
    public PlayerCharacter plCharacter;
    [HideInInspector]
    public InputHandler inputHandler;
    [HideInInspector]
    public IEvent3D interactibleTarget = null;
    [HideInInspector]
    public Animator myAnimator = null;

    CameraManager camManager;

    float delta;

    private void Awake() {
        
        GameManager.Instance.player = this;
        minimapMarker.SetActive(true);
        manLight.SetActive(true);
        womenLight.SetActive(false);
        manModel.SetActive(true);
        womenModel.SetActive(false);
        myAnimator = manModel.GetComponent<Animator>();
        changeSexParticles.SetActive(false);
        starParticles.SetActive(false);
    }

    void Start() {
        states = GetComponent<StateManager>();
        states.Init();

        inputHandler = GetComponent<InputHandler>();
        inputHandler.Init(this);

        plCharacter = GetComponent<PlayerCharacter>();
        plCharacter.Init();

        camManager = GameManager.Instance.cameraManager;
        camManager.Init(this);
    }

    // Update is called once per frame
    void Update() {
        inputHandler.Tick(delta);
    }

    private void FixedUpdate() {
        delta = Time.fixedDeltaTime;

        states.Tick(delta);
        camManager.Tick(delta);
    }

    public void ChangeSex() {
        changeSexParticles.SetActive(false);
        changeSexParticles.SetActive(true);

        plCharacter.man = !plCharacter.man;
        if (plCharacter.man) {
            manLight.SetActive(true);
            womenLight.SetActive(false);
            manModel.SetActive(true);
            womenModel.SetActive(false);

            myAnimator = manModel.GetComponent<Animator>();
        } else {
            manLight.SetActive(false);
            womenLight.SetActive(true);
            manModel.SetActive(false);
            womenModel.SetActive(true);

            myAnimator = womenModel.GetComponent<Animator>();
        }        
    }

    public void DoStartParticles() {
        starParticles.SetActive(false);
        starParticles.SetActive(true);
    }
}

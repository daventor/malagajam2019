﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[RequireComponent(typeof(Camera))]
public class CameraManager : MonoBehaviour {

    #region Variables
    public Vector3 offset;
    public float damp;    

    public float divisor = 10;
    public float minZoom = 60;
    public float maxZoom = 120;

    public float divisorZ = 10;
    public float minZoomZ = 60;
    public float maxZoomZ = 120;

    public float enmDistance = 10;    

    PlayerManager plManager;
    Vector3 velocity;

    public bool cameraIsometric;

    PostProcessVolume m_Volume;
    ColorGrading m_cGrading;
    #endregion

    #region Unity Methods

    public void Init(PlayerManager _plManager) {
        plManager = _plManager;

        m_cGrading = ScriptableObject.CreateInstance<ColorGrading>();
        m_cGrading.enabled.Override(true);
        m_cGrading.saturation.Override(-50);

        m_Volume = PostProcessManager.instance.QuickVolume(gameObject.layer, 100f, m_cGrading);
    }

    public void Tick(float delta) {        
        Move();
    }
    
    #endregion    

    void Move() {
        Vector3 centerPoint = plManager.transform.position;
        centerPoint += offset;
        transform.position = Vector3.SmoothDamp(transform.position, centerPoint, ref velocity, damp);
    }

    public void SetSaturation(float newSaturation) {        
        newSaturation = Mathf.Clamp(newSaturation, -100, 0);
        m_cGrading.saturation.Override(newSaturation);
        m_Volume = PostProcessManager.instance.QuickVolume(8, 100f, m_cGrading);        
    }
}
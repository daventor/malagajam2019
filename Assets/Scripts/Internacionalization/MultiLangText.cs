using UnityEngine;
using TMPro;
using System;
[RequireComponent(typeof(TextMeshProUGUI))]
public class MultiLangText : MonoBehaviour
{
    public string key;

	private TextMeshProUGUI textComponent;

	public static Action OnLanguageWasChanged = delegate { };
	private void Awake()
	{
		textComponent = GetComponent<TextMeshProUGUI>();
		OnLanguageWasChanged += LoadText;
	}
	private void Start()
	{
		LoadText();
	}

    public void LoadText()
	{
		if (LanguageManager.instance != null)
		{
			string textToSet = LanguageManager.instance.GetLocalizedValue(key);

			this.gameObject.GetComponent<TextMeshProUGUI>().text = textToSet;
		}
    }
}
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LanguageManager : MonoBehaviour
{
    public static LanguageManager instance;

	private Dictionary<string, string> localizedTexts;
	[Header("Idioma predeterminado")]
	[SerializeField]
    private string language;
	[SerializeField]
	private string MissingTextString = "Localized text not found";

	void Awake()
    {
		#region Singleton
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
		#endregion

		LoadLocalizedText();
	}

	public void SetLanguage(string lang)
    {
        language = lang;
		LoadLocalizedText();
		MultiLangText.OnLanguageWasChanged();
	}

    public string GetLocalizedValue(string key)
    {
        string result = MissingTextString;
        if (localizedTexts.ContainsKey(key))
        {
            result = localizedTexts[key];
        }
        else
        {
            Debug.LogErrorFormat("Language key not found! Keycode: {0}", key);
        }
        return result;
    }

	private void LoadLocalizedText()
	{
		localizedTexts = new Dictionary<string, string>();

		string filePath = "Assets/Resources/Languages/" + language + ".json"; //Path.Combine("/Resources/Languages/", "es.json");
		if (File.Exists(filePath))
		{
			string dataAsJson = File.ReadAllText(filePath);
			LangData loadedData = JsonUtility.FromJson<LangData>(dataAsJson);

			for (int i = 0; i < loadedData.texts.Length; i++)
			{
				localizedTexts.Add(loadedData.texts[i].key, loadedData.texts[i].value);
			}
		}
		else
		{
			Debug.LogError("Cannot find file location!");
		}
	}

}
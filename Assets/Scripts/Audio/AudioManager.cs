﻿using UnityEngine.Audio;
using System;
using UnityEngine;
using DG.Tweening;

public class AudioManager : MonoBehaviour {

    #region Variables
    public AudioMixerGroup musicGroup;
    public AudioMixerGroup soundGroup;
    public Sound[] sounds;    
    #endregion

    #region Unity Methods

    void Awake() {
        foreach (Sound s in sounds) {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;

            if (s.type == Sound.SoundType.sound) {
                s.source.outputAudioMixerGroup = soundGroup;
            } else {
                s.source.outputAudioMixerGroup = musicGroup;
            }
        }
    }

    public void Play(string sound) {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null) {
            Debug.LogWarning("Sound: " + sound + " not found!");
            return;
        }

        s.source.volume = s.volume * (1f + UnityEngine.Random.Range(-s.volumeVariance / 2f, s.volumeVariance / 2f));
        s.source.pitch = s.pitch * (1f + UnityEngine.Random.Range(-s.pitchVariance / 2f, s.pitchVariance / 2f));

        s.source.DOKill(true);
        if (s.type == Sound.SoundType.sound) s.source.PlayOneShot(s.clip);
        else s.source.Play();
    }

    public void StopAll() {
        foreach (Sound s in sounds) {
            if (s.source.isPlaying) {
                s.source.DOFade(0, 1).SetUpdate(true).OnComplete(() => { s.source.Stop(); });
            }
        }
    }

    public void Stop(string sound) {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s == null) {
            Debug.LogWarning("Sound: " + sound + " not found!");
            return;
        }

        if (s.fadeOut) {
            s.source.DOKill();
            s.source.DOFade(0, s.fadeOutTime).SetUpdate(true).OnComplete(() => { s.source.Stop(); });
        } else {
            s.source.Stop();
        }        
    }

    public void CrossFade(string soundA, string soundB) {
        Sound sA = Array.Find(sounds, item => item.name == soundA);
        Sound sB = Array.Find(sounds, item => item.name == soundB);

        if(sA != null && sB != null) {
            sA.source.DOKill();
            sB.source.DOKill();
            sA.source.DOFade(0, 3).SetUpdate(true);
            sB.source.DOFade(1, 3).SetUpdate(true);
        }
    }

    public void ChangeVolume(string sound, float newVolume) {
        Sound s = Array.Find(sounds, item => item.name == sound);
        if (s != null) {
            s.source.DOKill();
            s.source.DOFade(newVolume, 2).SetUpdate(true);
        }
    }

    #endregion
}
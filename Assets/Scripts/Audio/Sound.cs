﻿/*
* Copyright Deimos Studio
* Author: Daventor (davelop@gmail.com)
*/

using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound {
    public enum SoundType {
        sound,
        music
    }

    public string name;

    public AudioClip clip;

    public SoundType type;

    [Range(0f, 1f)]
    public float volume = 1;
    [Range(0f, 1f)]
    public float volumeVariance;

    [Range(-2f, 2f)]
    public float pitch = 1;
    [Range(0f, 1f)]
    public float pitchVariance;

    public bool loop;
    public bool fadeOut;
    [Range(0f, 2f)]
    public float fadeOutTime = 0.5f;

    [HideInInspector]
    public AudioSource source;
}

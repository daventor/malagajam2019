﻿using UnityEngine;

[System.Serializable]
public struct Result
{
	[Range(-1, 1)]
	public float money;
	[Range(-1, 1)]
	public float social;
	[Range(-1, 1)]
	public float family;
	[Range(-1, 1)]
	public float happiness;
}

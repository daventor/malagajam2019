﻿public enum GameStateType {
    inGame,    
    inMenu,    
    inPause,    
    inGameOver
}
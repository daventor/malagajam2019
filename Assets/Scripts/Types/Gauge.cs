﻿public struct Gauge
{
	public readonly float MaxValue;

	public Gauge(float initialValue, float maxValue)
	{
		MaxValue = maxValue;
		currentValue = initialValue;
	}

	public float CurrentValue
	{
		get
		{
			return currentValue;
		}
		set
		{
			currentValue = value > MaxValue ? MaxValue : value;
			currentValue = currentValue < 0 ? 0 : CurrentValue;

		}
	}
	private float currentValue;
}

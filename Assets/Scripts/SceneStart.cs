﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Instance.audioManager.StopAll();
        GameManager.Instance.Play("StartBGMusic");
        DOVirtual.DelayedCall(4.038f, () => { GameManager.Instance.Play("BGMusicGood"); });
        DOVirtual.DelayedCall(4.038f, () => { GameManager.Instance.Play("BGMusicBad"); });        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using UnityEngine;
using DG.Tweening;

public class InGameInfo : MonoBehaviour {

    #region Variables    
    CanvasGroup canvas;
    #endregion

    #region Unity Methods

    void Start() {
        canvas = GetComponent<CanvasGroup>();
        canvas.alpha = 0f;
    }

    void Update() {
        if (canvas.alpha > 0) {
            Vector3 targetPos = transform.position + GameManager.Instance.mainCamera.transform.rotation * Vector3.forward;
            Vector3 targetOrientation = GameManager.Instance.mainCamera.transform.rotation * Vector3.up;
            transform.LookAt(targetPos, targetOrientation);
        }
    }

    public void FadeIn() {
        canvas.DOKill();
        canvas.DOFade(1f, 0.5f).SetEase(Ease.Linear);
    }

    public void FadeOut() {
        canvas.DOKill();
        canvas.DOFade(0f, 0.5f).SetEase(Ease.Linear);
    }

    #endregion
}


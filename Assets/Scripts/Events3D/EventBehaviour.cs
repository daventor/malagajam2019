﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening; 

public class EventBehaviour : IEvent3D {
    public string locationId;

    public InGameInfo myInfo;
    public InGameInfo myActive;
    public Image timerBar;
    public GameObject execPanel;
    public Image execBar;
    public GameObject btnLabelPanel;
    public Transform minimapMarker;

    public string execSoundName;

    bool showPanel = false;
    bool inExecution = false;

    Event myEvent;
    Event lastEvent = null;

    private void Start() {
        btnLabelPanel.transform.DOScale(1.25f, 0.25f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);        
        minimapMarker.gameObject.SetActive(false);
    }

    private void Update() {
        if (inExecution) return;
        myEvent = GameManager.Instance.eventManager.GetEventByLocationID(locationId);        

        if (!showPanel && myEvent != null && myEvent != lastEvent) {            
            lastEvent = myEvent;
            myActive.FadeIn();

            GameManager.Instance.Play("Notification");
            btnLabelPanel.SetActive(true);
            execPanel.SetActive(false);
            timerBar.DOKill();
            timerBar.transform.parent.DOKill();
            timerBar.fillAmount = execBar.fillAmount = 0;            
            timerBar.DOFillAmount(1, myEvent.timeout).SetEase(Ease.Linear);            
            timerBar.transform.parent.DOScale(1f, 0);
            timerBar.transform.parent.DOScale(1.25f, 0.125f).SetDelay(myEvent.timeout - (myEvent.timeout * 0.25f)).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
            minimapMarker.gameObject.SetActive(true);
        } else if(myEvent == null && myEvent != lastEvent) {
            lastEvent = null;
            myActive.FadeOut();
            myInfo.FadeOut();
            minimapMarker.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other) {
        showPanel = true;
        if (myEvent == null) return;
        
        if (other.gameObject == GameManager.Instance.player.gameObject) {            
            myInfo.FadeIn();
            GameManager.Instance.player.interactibleTarget = this;
        }
    }

    void OnTriggerExit(Collider other) {
        showPanel = false;
        if (myEvent == null) return;

        if (other.gameObject == GameManager.Instance.player.gameObject) {
            myInfo.FadeOut();            
            GameManager.Instance.player.interactibleTarget = null;
        }
    }

    public override void ActiveAction() {
        if (myEvent == null) return;
        if (inExecution) return;

        myActive.FadeOut();
        GameManager.Instance.Play(execSoundName);
        execPanel.SetActive(true);
        btnLabelPanel.SetActive(false);
        inExecution = true;
        GameManager.Instance.player.canControl = false;
        execBar.fillAmount = 0;
        GameManager.Instance.eventManager.RemoveEvent(myEvent);
        execBar.DOFillAmount(1, myEvent.executionTime).SetEase(Ease.Linear).OnComplete(() => {            
            GameManager.Instance.eventManager.SuccessEvent(myEvent);
            inExecution = false;
            GameManager.Instance.player.canControl = true;
            myInfo.FadeOut();                        
            GameManager.Instance.Stop(execSoundName);
            timerBar.DOKill();
            timerBar.transform.parent.DOScale(1f, 0);
            // Launch particles
            GameManager.Instance.player.DoStartParticles();
        });        
    }
}
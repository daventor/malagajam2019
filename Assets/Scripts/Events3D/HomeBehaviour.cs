﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening; 

public class HomeBehaviour : IEvent3D {  
    public InGameInfo myInfo;    
    public GameObject btnLabelPanel;
    public Transform minimapMarker;
    public string execSoundName;

    private void Start() {
        btnLabelPanel.transform.DOScale(1.25f, 0.25f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);        
        minimapMarker.gameObject.SetActive(true);
    }    

    void OnTriggerEnter(Collider other) {                        
        if (other.gameObject == GameManager.Instance.player.gameObject) {            
            myInfo.FadeIn();
            GameManager.Instance.player.interactibleTarget = this;
        }
    }

    void OnTriggerExit(Collider other) {        
        if (other.gameObject == GameManager.Instance.player.gameObject) {
            myInfo.FadeOut();            
            GameManager.Instance.player.interactibleTarget = null;
        }
    }

    public override void ActiveAction() {
        GameManager.Instance.player.ChangeSex();
        GameManager.Instance.Play(execSoundName);
    }
}
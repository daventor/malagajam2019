﻿using UnityEngine;

public abstract class IEvent3D : MonoBehaviour
{
    public abstract void ActiveAction();
}
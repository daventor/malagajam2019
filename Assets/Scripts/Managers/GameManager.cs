﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	#region Variables    
	public static GameManager Instance;
	public GameStateType currentGameState;
    public PlayerManager player;
	public EventManager eventManager;
	public GameplaySettings settings;
	public GameStateManager gameStateManager;
	public InGameCounter inGameCounter;
	public StatGUIManager statGUIManager;
	public EventGUIManager eventGUIManager;
	public GameOverGUIManager gameOverGUIManager;
	[HideInInspector]
    public AudioManager audioManager;

	private CameraManager _cameraManager;
	public CameraManager cameraManager {
        get {
            if (_cameraManager == null) _cameraManager = mainCamera.GetComponent<CameraManager>();
            return _cameraManager;
        }
    }

    private Camera _mainCamera;
    public Camera mainCamera {
        get {
            if (_mainCamera == null) _mainCamera = Camera.main;
            return _mainCamera;
        }
    }
    #endregion

    #region Unity Methods

    void Awake() {
        if (Instance != null && Instance != this) {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);
        audioManager = GetComponent<AudioManager>();

        ApplySettings();

	}

	public void Play(string clipName) {
        audioManager.Play(clipName);
    }

    public void Stop(string clipName) {
        audioManager.Stop(clipName);
    }

    public void ApplySettings() {
        audioManager.soundGroup.audioMixer.SetFloat("soundVolume", 0);
        audioManager.musicGroup.audioMixer.SetFloat("musicVolume", 0);
    }

	#endregion
}
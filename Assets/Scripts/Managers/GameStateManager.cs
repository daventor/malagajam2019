﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : MonoBehaviour
{
	public enum GameOverType { Money, Social, Family, Happiness, }

	public void Awake()
	{
		GameManager.Instance.gameStateManager = this;
	}
	public void GameOver(GameOverType gameOverType)
	{        
		Time.timeScale = 0;
        GameManager.Instance.audioManager.StopAll();
        GameManager.Instance.Play("BadEnd");
        GameManager.Instance.currentGameState = GameStateType.inGameOver;
		GameManager.Instance.gameOverGUIManager.GameOver(gameOverType);
	}

	public void Victory()
	{
		Time.timeScale = 0;
        GameManager.Instance.audioManager.StopAll();
        GameManager.Instance.Play("GoodEnd");
        GameManager.Instance.currentGameState = GameStateType.inGameOver;
		GameManager.Instance.gameOverGUIManager.Victory();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class HowToPlayGUIManager : MonoBehaviour
{
	[SerializeField]
	private Image panel;
	[SerializeField]
	private TextMeshProUGUI text;

	private bool canLoadLevel;

	[SerializeField]
	private TextMeshProUGUI goToMenuText;
	[SerializeField]
	[TextArea]
	private string howToGame;	

	public void Awake()
	{
//		GameManager.Instance.gameOverGUIManager = this;
	}

    public void Start() {
        Time.timeScale = 0;
        GameManager.Instance.currentGameState = GameStateType.inPause;
        GameManager.Instance.audioManager.StopAll();
        GameManager.Instance.Play("StartBGMusic");
        DOVirtual.DelayedCall(4.038f, () => { GameManager.Instance.Play("BGMusicGood"); });
        DOVirtual.DelayedCall(4.038f, () => { GameManager.Instance.Play("BGMusicBad"); });

        HowToGame();
    }

    public void HowToGame()
	{				
		text.text = howToGame;
		StartCoroutine(HowToPlayAnim());
	}
	public IEnumerator HowToPlayAnim()
	{		
		yield return new WaitForSecondsRealtime(0.5f);
		text.DOFade(1, 1).SetUpdate(true);

		yield return new WaitForSecondsRealtime(2f);
		goToMenuText.DOFade(1, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);

		canLoadLevel = true;
	}

	private void Update()
	{
		if (canLoadLevel&& Rewired.ReInput.players.GetPlayer(0).GetAnyButtonDown())
		{
            text.gameObject.SetActive(false);
            panel.DOFade(0, 0.5f).SetUpdate(true).OnComplete(()=> {
                panel.transform.parent.gameObject.SetActive(false);

                Time.timeScale = 1;
                GameManager.Instance.currentGameState = GameStateType.inGame;                
            });            
		}
	}
}

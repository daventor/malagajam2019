﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Linq;
using TMPro;
using UnityEngine.UI;
public class EventGUIManager : MonoBehaviour
{

	public const float timeShowingGender = 30f;
	[SerializeField]
	private RectTransform[] displays;

	[SerializeField]
	private Vector2 entryPosition;

	[SerializeField]
	private float movementX;

	[SerializeField]
	private float movementY;

	private List<RectTransform> activeEventDisplays;

	[SerializeField]
	private Sprite manSprite;
	[SerializeField]
	private Sprite womanSprite;
	[SerializeField]
	private Sprite unknownSprite;
	
	private bool shouldShowGender
	{
		get
		{
			return GameManager.Instance.inGameCounter.timeCounter + timeShowingGender > InGameCounter.GameTime;
		}
	}
	private void Awake()
	{
		GameManager.Instance.eventGUIManager = this;
		activeEventDisplays = new List<RectTransform>();
	}
	public void AddEvent(Event addingEvent)
	{
		StartCoroutine(AddDisplayAnimation(addingEvent));
	}
	//Lo siento.----
	public void RemoveEvent(Event removingEvent)
	{
		RectTransform display = activeEventDisplays.FirstOrDefault(x => x.GetComponentInChildren<TextMeshProUGUI>().text == removingEvent.description);
		if (display != null)
		{
			StartCoroutine(RemoveDisplayAnimation(display));
			activeEventDisplays.Remove(display);
		}
	}
	private IEnumerator RemoveDisplayAnimation(RectTransform displayTransform)
	{
		List<RectTransform> fallingDisplays =  activeEventDisplays.FindAll(x => x.anchoredPosition.y > displayTransform.anchoredPosition.y);

		displayTransform.DOAnchorPosX(displayTransform.anchoredPosition.x - movementX, 0.25f).SetEase(Ease.Linear).WaitForKill();
		yield return new WaitForSeconds(0.25f);

		for (int a = 0; a < fallingDisplays.Count; a++)
		{
			fallingDisplays[a].DOAnchorPosY(fallingDisplays[a].anchoredPosition.y - movementY, 0.25f).SetEase(Ease.Linear).WaitForKill();
		}
	}
	//End lo siento
	private IEnumerator AddDisplayAnimation(Event addingEvent)
	{
		RectTransform displayTransform = displays.FirstOrDefault(x => !activeEventDisplays.Contains(x));
		displayTransform.GetChild(1).GetComponent<Image>().sprite = shouldShowGender ? (addingEvent.bestResultIsMan ? manSprite : womanSprite) : unknownSprite;
		if (displayTransform != null)
		{
			displayTransform.GetComponentInChildren<TextMeshProUGUI>().text = addingEvent.description;
			for (int a = 0; a < activeEventDisplays.Count; a++)
			{
				activeEventDisplays[a].DOAnchorPosY(activeEventDisplays[a].anchoredPosition.y + movementY, 0.25f).SetEase(Ease.Linear).WaitForKill();
			}
			activeEventDisplays.Add(displayTransform);
			displayTransform.anchoredPosition = entryPosition;

			yield return new WaitForSeconds(0.25f);
			displayTransform.DOAnchorPosX(displayTransform.anchoredPosition.x + movementX, 0.25f).SetEase(Ease.Linear).WaitForKill();
		}
		else
		{
			Debug.LogError("Displays no disponibles");
		}
	}
}

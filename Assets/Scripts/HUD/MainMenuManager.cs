﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class MainMenuManager : MonoBehaviour
{
	[SerializeField]
	private Image panel;

	private bool canLoadLevel;

	[SerializeField]
	private TextMeshProUGUI titleText;
	[SerializeField]
	private TextMeshProUGUI descriptionText;
	[SerializeField]
	private TextMeshProUGUI pressAnyButtonText;

	public void Start()
	{
		GameManager.Instance.currentGameState = GameStateType.inPause;
		GameManager.Instance.audioManager.StopAll();
		GameManager.Instance.Play("StartMenu");
		StartCoroutine(Title());
	}

	public IEnumerator Title()
	{
		titleText.DOFade(1, 1);
		yield return new WaitForSecondsRealtime(0.5f);
		descriptionText.DOFade(1, 1);

		yield return new WaitForSecondsRealtime(2f);
		pressAnyButtonText.DOFade(1, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);

		canLoadLevel = true;
	}

	private void Update()
	{
		if (canLoadLevel && Rewired.ReInput.players.GetPlayer(0).GetAnyButtonDown())
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(1);
			GameManager.Instance.Play("Notification");
		}
	}
}

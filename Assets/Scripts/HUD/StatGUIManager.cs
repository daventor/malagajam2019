﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class StatGUIManager : MonoBehaviour
{
	private readonly float fillTime = 0.5f;
	[SerializeField]
	private Image moneyImage;
	[SerializeField]
	private Image familyImage;
	[SerializeField]
	private Image socialImage;
	[SerializeField]
	private Image happinessImage;

	private PlayerCharacter playerCharacter;
	private void Awake()
	{
		GameManager.Instance.statGUIManager = this;
	}
	private void Start()
	{
		playerCharacter = GameManager.Instance.player.plCharacter;

		moneyImage.fillAmount = playerCharacter.money.CurrentValue;
		familyImage.fillAmount = playerCharacter.family.CurrentValue;
		socialImage.fillAmount = playerCharacter.social.CurrentValue;
		happinessImage.fillAmount = playerCharacter.happiness.CurrentValue;
	}
	public void UpdateSliders()
	{
		moneyImage.DOKill();
		familyImage.DOKill();
		socialImage.DOKill();
		happinessImage.DOKill();
		moneyImage.DOFillAmount(playerCharacter.money.CurrentValue, fillTime).SetUpdate(true); 
		familyImage.DOFillAmount(playerCharacter.family.CurrentValue, fillTime).SetUpdate(true);
		socialImage.DOFillAmount(playerCharacter.social.CurrentValue, fillTime).SetUpdate(true);
		happinessImage.DOFillAmount(playerCharacter.happiness.CurrentValue, fillTime).SetUpdate(true);
	}
}

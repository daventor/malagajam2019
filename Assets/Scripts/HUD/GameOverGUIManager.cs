﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class GameOverGUIManager : MonoBehaviour
{
	[SerializeField]
	private Image panel;
	[SerializeField]
	private TextMeshProUGUI text;

	private bool canLoadLevel;

	[SerializeField]
	private TextMeshProUGUI goToMenuText;
	[SerializeField]
	[TextArea]
	private string moneyEndingText;
	[SerializeField]
	[TextArea]
	private string socialEndingText;
	[SerializeField]
	[TextArea]
	private string familyEndingText;
	[SerializeField]
	[TextArea]
	private string happinessEndingText;
	[SerializeField]
	[TextArea]
	private string victoryText;

	public void Awake()
	{
		GameManager.Instance.gameOverGUIManager = this;
	}
	public void GameOver(GameStateManager.GameOverType gameOverType)
	{
		switch (gameOverType)
		{
			case GameStateManager.GameOverType.Money: text.text = moneyEndingText;
				break;
			case GameStateManager.GameOverType.Social: text.text = socialEndingText;
				break;
			case GameStateManager.GameOverType.Family: text.text = familyEndingText;
				break;
			case GameStateManager.GameOverType.Happiness: text.text = happinessEndingText;
				break;
		}

		StartCoroutine(GameOverAnim());
	}

	public void Victory()
	{
		text.text = victoryText;
		StartCoroutine(GameOverAnim());
	}
	public IEnumerator GameOverAnim()
	{
		panel.DOFade(1, 0.5f).SetUpdate(true);
		yield return new WaitForSecondsRealtime(0.5f);
		text.DOFade(1, 1).SetUpdate(true);

		yield return new WaitForSecondsRealtime(2f);
		goToMenuText.DOFade(1, 1f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);

		canLoadLevel = true;
	}

	private void Update()
	{
		if (canLoadLevel&& Rewired.ReInput.players.GetPlayer(0).GetAnyButtonDown())
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		}
	}
}

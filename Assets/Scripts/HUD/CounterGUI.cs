﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CounterGUI : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI text;

	private void Update()
	{
		text.text = ((int)GameManager.Instance.inGameCounter.timeCounter).ToString();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gameplay settings", menuName = "Malagajam2019/New Gameplay settings", order = 0)]
public class GameplaySettings : ScriptableObject
{
	public float minEventCreationTime;

	public float levelUpTime;

	public int maxEventsAtOnce;

	[SerializeField]
	private float initialEventCreationTime;

	[SerializeField]
	private float eventCreationTimeModifier;

	public float GetEventCreationTime()
	{
			return Mathf.Clamp(initialEventCreationTime - (GameManager.Instance.inGameCounter.level * eventCreationTimeModifier), minEventCreationTime, initialEventCreationTime);
	}
}

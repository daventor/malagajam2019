﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameCounter : MonoBehaviour
{
	public int level;

	public const float GameTime = 250;

	public float timeCounter;
	private void Awake()
	{
		GameManager.Instance.inGameCounter = this;
		level = 1;
		timeCounter = GameTime;
	}
	private void Start()
	{
		InvokeRepeating("LevelUp", GameManager.Instance.settings.levelUpTime, GameManager.Instance.settings.levelUpTime);
	}
	private void Update()
	{
		if (GameManager.Instance.currentGameState == GameStateType.inGame)
		{
			timeCounter -= Time.deltaTime;
			if (timeCounter <= 0)
			{
				GameManager.Instance.gameStateManager.Victory();
			}
		}
	}
	private void LevelUp()
	{
		level++;
	}
}

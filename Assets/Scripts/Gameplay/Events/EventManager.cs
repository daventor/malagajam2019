﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class EventManager : MonoBehaviour
{

	[SerializeField]
	private EventList eventList;

	[SerializeField]
	private List<Event> activeEvents;
	private bool CanCreateEvents
	{
		get
		{
			return activeEvents.Count < GameManager.Instance.settings.maxEventsAtOnce && GameManager.Instance.currentGameState == GameStateType.inGame; //Temporal
		}
	}
	private void Awake()
	{
		activeEvents = new List<Event>();
		GameManager.Instance.eventManager = this;
	}
	private void Start()
	{
		ActivateEvent();
		StartCoroutine("EventCreationQueue");
	}
	private IEnumerator EventCreationQueue()
	{
		while (true)
		{
			float counter = GameManager.Instance.settings.GetEventCreationTime();
			while (CanCreateEvents)
			{
				counter -= Time.deltaTime;
				if (counter <= 0)
				{
					counter = GameManager.Instance.settings.GetEventCreationTime();
					ActivateEvent();
				}
				yield return null;
			}
			yield return null;
		}
	}
	public void SuccessEvent(Event eventSuccessed)
	{
        GameManager.Instance.Play("EventOk");
		Result result = GameManager.Instance.player.plCharacter.man ? eventSuccessed.manResult : eventSuccessed.womanResult; 
		GameManager.Instance.player.plCharacter.ChangePlayerValues(result);
	}
	public void EventFailure(Event eventFailed)
	{
		RemoveEvent(eventFailed);
        GameManager.Instance.Play("EventKo");
        GameManager.Instance.player.plCharacter.ChangePlayerValues(eventFailed.failureResult);
	}
	public void RemoveEvent(Event eventToRemove)
	{
		activeEvents.Remove(eventToRemove);
		GameManager.Instance.eventGUIManager.RemoveEvent(eventToRemove);
	}
	public Event GetEventByLocationID(string locationId)
	{
		return activeEvents.FirstOrDefault(x => x.locationID == locationId);
	}

	private List<Event> GetAvailableEvents()
	{
		List<Event> availableEvents = eventList.eventList;

		for (int a = 0; a < activeEvents.Count; a++)
		{
			availableEvents = availableEvents.Except(eventList.eventList.FindAll(x => x.locationID == activeEvents[a].locationID)).ToList<Event>();
		}
		return availableEvents;
	}
	private void ActivateEvent()
	{
		List<Event> availableEvents = GetAvailableEvents();
		Event eventToActivate = availableEvents[Random.Range(0, availableEvents.Count)];

		ActivateEvent(eventToActivate);
	}
	private void ActivateEvent(Event eventToActivate)
	{
		activeEvents.Add(eventToActivate);
		GameManager.Instance.eventGUIManager.AddEvent(eventToActivate);
		StartCoroutine(RemoveEvent(eventToActivate, eventToActivate.timeout));
	}
	private IEnumerator RemoveEvent(Event eventToRemove, float time)
	{
		float counter = 0f;
		while (activeEvents.Contains(eventToRemove))
		{
			counter += Time.deltaTime;

			if (counter >= time)
			{
				RemoveEvent(eventToRemove);
				EventFailure(eventToRemove);
			}
			yield return null;
		}
	}
}

﻿using UnityEngine;

[System.Serializable]
public class Event 
{
	public bool bestResultIsMan;
	public string description;
	public string locationID;
	public float timeout;
	public float executionTime;
	public Result manResult;
	public Result womanResult;
	public Result failureResult;
}

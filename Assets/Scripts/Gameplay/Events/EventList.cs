﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event", menuName = "Malagajam2019/New event", order = 0)]
public class EventList : ScriptableObject
{
	public List<Event> eventList;
}
